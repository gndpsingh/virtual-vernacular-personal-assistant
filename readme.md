# Running the server

## prerequisites
 - python 3
 - pip install -r requirements.txt

## commands to run server:
```export FLASK_APP=hello.py```

```flask run --host=0.0.0.0```

# Android Client Repository
https://bitbucket.org/gndpsingh/assistant-android/src/master/