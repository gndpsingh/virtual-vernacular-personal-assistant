from flask import Flask
from flask import request
import subprocess

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/upload', methods=['POST'])
def upload_file():
    print(request.files)
    # checking if the file is present or not.
    if 'file' not in request.files:
        return 'No file found'

    file = request.files['file']
    filename = request.values.get("filename")
    print('saving ' + filename + ' ..')
    file.save(filename)
    return 'file successfully saved'

@app.route('/detect', methods=['POST'])
def detect_file():
    print(request.files)
    # checking if the file is present or not.
    if 'file' not in request.files:
        return 'No file found'

    file = request.files['file']
    filename = request.values.get("filename")
    print('saving ' + filename + ' ..')
    file.save(filename)
    try:
        run_detection(filename)
    except:
        print ("problem in run_detection")

    output = 'default'
    try:
        f = open('result.txt','r')
        output = f.readlines()[0]
    except:
        print('problem in reading result')
    return output

def run_detection(filename):
    python3_command = "snowboy/run_hotword_detection_with_file.py"
    script = ["python2.7", python3_command]
    process = subprocess.Popen(" ".join(script), shell=True, env={"PYTHONPATH": "."})
    process.wait()
